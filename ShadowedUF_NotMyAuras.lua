local ADDON_NAME, PRIVATE_TABLE = ...
local playerUnits = {player = true, vehicle = true, pet = true}
local canCure = ShadowUF.Units.canCure
local Auras = ShadowUF.modules["auras"]
local defaultUpdate = ShadowUF.modules.auras.Update

-- Wrap the DevTool addon function in a wrapper for easier toggling
local function debugLog(param1, param2)

	-- Making DevTools handle just 1 param if I want to send a value but not a key for simple tests
	local myParam1 = param1 or param2
	local myParam2 = param2 or ""
	-- Uncomment the following line to see debugging with /devtool (https://www.curseforge.com/wow/addons/devtool)
	--DevTool:AddData(myParam1, myParam2)

end

-- Modify an aura's appearance based on the config that has been passed
local function modifyAura(button, config)

	local fadeout = config.fadeOut or false
	local fadeAmount = config.fadeAmount or 0
	local desaturate = config.desaturate or false
	local darken = config.darken or false
	local darkenAmount = config.darkenAmount or 0

	-- Reversing the slider value so we can use it for the vertex color
	if (darkenAmount == 0) then
		darkenAmount = 1
	elseif (darkenAmount > 0) then
		darkenAmount = 1 - darkenAmount
	end

	if (config.fadeOut == true) then
		button:SetAlpha(fadeAmount)
	else
		button:SetAlpha(1)
	end

	if (config.desaturate == true) then
		button.icon:SetDesaturated(1)
	else
		button.icon:SetDesaturated(nil)
	end

	if (config.darken == true) then
		button.icon:SetVertexColor(darkenAmount, darkenAmount, darkenAmount)
	else
		button.icon:SetVertexColor(1, 1, 1)
	end

end

-- Reset an aura's appearance
local function resetAura(button)

	button:SetAlpha(1)
	button.icon:SetDesaturated(nil)
	button.icon:SetVertexColor(1, 1, 1)

end

-- Check the auras for an aura frame, and modify them if they're not the player's auras
local function checkAuras(parent, auraFrame, config)

	local auraFrame = auraFrame or false

	-- Confirm the Aura Frame
	if (auraFrame ~= false) then
		debugLog(auraFrame, "Aura Frame")
	else
		debugLog("No Aura Frame")
		return false
	end

	-- If there are no auras to handle, just bail
	if (auraFrame.totalAuras <= 0) then
		debugLog(auraFrame, "No Auras")
		return false
	else
		debugLog(auraFrame.totalAuras, "Total Auras")
	end

	-- The auraFrame.buttons array is not zero indexed, and starts with auraFrame.buttons[1], so start it at 1
	auraFrame.index = 1

	-- Loop through all the auras in this frame
	while ( auraFrame.index <= auraFrame.totalAuras) do

		-- Should never happen, but defensive checks
		if (auraFrame.buttons[auraFrame.index] == nil) then
			debugLog("No button at that index")
			return false
		end

		-- Set a default flag to true, assuming the Aura will be modified, then run various checks to see if we need to exclude the Aura
		local shouldModify = true

		-- auraID is the true index of the aura on a player, while the button index is only of visible buttons, so we need to use the auraID for the UnitAura function call
		local thisAuraId = auraFrame.buttons[auraFrame.index].auraID

		-- Debuffs can be put into the Buffs frame when both frames have the same anchor position
		local thisAuraFilter = auraFrame.buttons[auraFrame.index].filter

		-- Load the appropriate config for this aura type, defaulting to buffs
		local thisAuraConfig = config.buffs
		if (thisAuraFilter == "HARMFUL") then
			thisAuraConfig = config.debuffs
		end

		-- Get the Aura information returned from SUF's UnitAura function
		local name, texture, count, auraType, duration, endTime, caster, isRemovable, nameplateShowPersonal, spellID, canApplyAura, isBossDebuff = UnitAura(auraFrame.parent.unit, thisAuraId, thisAuraFilter)

		-- Check if the current frame is on a unit friendly to the player
		local isFriendly = not UnitIsEnemy(auraFrame.parent.unit, "player")

		-- Should never happen, but defensive checks
		if (name == nil) then
			debugLog("Aura had no name")
			return false;
		end

		-- Caster information purely for debugging
		if (caster) then
			debugLog(name.." (by "..caster..")", "Aura "..auraFrame.index)
		else
			debugLog(name.." (no caster?)", "Aura "..auraFrame.index)
		end

		-- Check the aura against filters: these are redundancies as SUF should have filtered these out of the buttons array anyway
		if (parent.whitelist[auraType] and not parent.whitelist[name] and not parent.whitelist[spellID]) then
			shouldModify = false
			debugLog(name.." not on whitelist")
		end

		if (parent.blacklist[auraType] and (parent.blacklist[name] or parent.blacklist[spellID])) then
			shouldModify = false
			debugLog(name.." on blacklist")
		end

		-- Check if the aura is cast by the player
		if (caster == "player") then
			shouldModify = false
			debugLog(name.." caster is player")
		end

		-- Check if this is a boss aura, and if we should be modifying boss auras
		if (isBossDebuff and thisAuraConfig.ignoreBoss) then
			shouldModify = false
			debugLog(name.." is boss debuff, and ignoring boss debuffs")
		end

		-- Check if this is aura can be removed, and if we should be modifying removable auras
		if (not isFriendly and isRemovable and thisAuraConfig.ignoreRem) then
			shouldModify = false
			debugLog(name.." is removable, and ignoring removable")
		end

		-- Check if this is aura can be cured, and if we should be modifying removable auras
		if (isFriendly and thisAuraFilter == "HARMFUL" and canCure[auraType] and thisAuraConfig.ignoreRem) then
		    shouldModify = false
		    debugLog(name.." is removable (curable), and ignoring removable")
		end

		if (shouldModify) then
			-- If we still think the aura should be modified, pass the button and the unit's config
			modifyAura(auraFrame.buttons[auraFrame.index], thisAuraConfig)
		else
			-- If something has flagged this aura not to be modified, make sure the aura's appearance is reset
			resetAura(auraFrame.buttons[auraFrame.index])
		end

		-- Increment by one to continue looping through the auras
		auraFrame.index = auraFrame.index + 1

	end

end

-- Hook into SUF's aura update function
function ShadowUF.modules.auras:Update(frame, ...)

	-- Get the config for this unit
	local config = ShadowUF.db.profile.units[frame.unitType].notMyAuras

	-- Run SUF's aura update function first
	defaultUpdate(self, frame, ...)

	-- Pass each aura frame (buffs and debuffs) to be checked
	if (frame.auras) then
		if (frame.auras.buffs) then
			checkAuras(frame.auras, frame.auras.buffs, config)
		end
		if (frame.auras.debuffs) then
			checkAuras(frame.auras, frame.auras.debuffs, config)
		end
	end


end

-- Add our config options to SUF
function Auras:OnConfigurationLoad()

	local auraArgs = ShadowUF.Config.auraTable.args.display.args

	auraArgs.notMyAuras = {
		order = 5,
		type = "group",
		inline = true,
		hidden = false,
		args = {
			buffs = {
				order = 1,
				type = "group",
				name = "Not My Auras",
				inline = true,
				hidden = function(info)
					local type = info[#(info) - 3]
					if (type == "buffs") then
						return false
					else
						return true
					end
				end,
				disabled = function(info)
					local type = info[#(info) - 4]
					if (type == "buffs") then
						return false
					else
						return true
					end
				end,
				args = {
					fadeOut = {
						order = 0,
						type = "toggle",
						name = PRIVATE_TABLE.L["FadeBuffTitle"],
						desc = PRIVATE_TABLE.L["FadeBuffDesc"],
						arg = "notMyAuras.buffs.fadeOut",
						hidden = false,
					},
					fadeAmount = {
						order = 1,
						type = "range",
						name = PRIVATE_TABLE.L["FadeOpacBuffTitle"],
						desc = PRIVATE_TABLE.L["FadeOpacBuffDesc"],
						min = 0,
						max = 1,
						step = 0.05,
						isPercent = true,
						arg = "notMyAuras.buffs.fadeAmount",
						hidden = false,
					},
					darken = {
						order = 2,
						type = "toggle",
						name = PRIVATE_TABLE.L["DarkBuffTitle"],
						desc = PRIVATE_TABLE.L["DarkBuffDesc"],
						arg = "notMyAuras.buffs.darken",
						hidden = false,
					},
					darkenAmount = {
						order = 3,
						type = "range",
						name = PRIVATE_TABLE.L["DarkAmountBuffTitle"],
						desc = PRIVATE_TABLE.L["DarkAmountBuffDesc"],
						min = 0,
						max = 1,
						step = 0.05,
						isPercent = true,
						arg = "notMyAuras.buffs.darkenAmount",
						hidden = false,
					},
					desaturate = {
						order = 4,
						inline = false,
						type = "toggle",
						name = PRIVATE_TABLE.L["DesatBuffTitle"],
						desc = PRIVATE_TABLE.L["DesatBuffDesc"],
						arg = "notMyAuras.buffs.desaturate",
						hidden = false,
					},
					ignoreBoss = {
						order = 5,
						type = "toggle",
						name = PRIVATE_TABLE.L["IgnoreBossBuffTitle"],
						desc = PRIVATE_TABLE.L["IgnoreBossBuffDesc"],
						arg = "notMyAuras.buffs.ignoreBoss",
						hidden = false,
					},
					IgnoreRem = {
						order = 6,
						type = "toggle",
						name = PRIVATE_TABLE.L["IgnoreRemBuffTitle"],
						desc = PRIVATE_TABLE.L["IgnoreRemBuffDesc"],
						arg = "notMyAuras.buffs.ignoreRem",
						hidden = false,
					}
				},
			},
			debuffs = {
				order = 2,
				type = "group",
				name = "Not My Auras",
				inline = true,
				hidden = function(info)
					local type = info[#(info) - 3]
					if (type == "debuffs") then
						return false
					else
						return true
					end
				end,
				disabled = function(info)
					local type = info[#(info) - 4]
					if (type == "debuffs") then
						return false
					else
						return true
					end
				end,
				args = {
					fadeOut = {
						order = 0,
						type = "toggle",
						name = PRIVATE_TABLE.L["FadeDebuffTitle"],
						desc = PRIVATE_TABLE.L["FadeDebuffDesc"],
						arg = "notMyAuras.debuffs.fadeOut",
						hidden = false,
					},
					fadeAmount = {
						order = 1,
						type = "range",
						name = PRIVATE_TABLE.L["FadeOpacDebuffTitle"],
						desc = PRIVATE_TABLE.L["FadeOpacDebuffDesc"],
						min = 0,
						max = 1,
						step = 0.05,
						isPercent = true,
						arg = "notMyAuras.debuffs.fadeAmount",
						hidden = false,
					},
					darken = {
						order = 2,
						type = "toggle",
						name = PRIVATE_TABLE.L["DarkDebuffTitle"],
						desc = PRIVATE_TABLE.L["DarkDebuffDesc"],
						arg = "notMyAuras.debuffs.darken",
						hidden = false,
					},
					darkenAmount = {
						order = 3,
						type = "range",
						name = PRIVATE_TABLE.L["DarkAmountDebuffTitle"],
						desc = PRIVATE_TABLE.L["DarkAmountDebuffDesc"],
						min = 0,
						max = 1,
						step = 0.05,
						isPercent = true,
						arg = "notMyAuras.debuffs.darkenAmount",
						hidden = false,
					},
					desaturate = {
						order = 4,
						inline = false,
						type = "toggle",
						name = PRIVATE_TABLE.L["DesatDebuffTitle"],
						desc = PRIVATE_TABLE.L["DesatDebuffDesc"],
						arg = "notMyAuras.debuffs.desaturate",
						hidden = false,
					},
					ignoreBoss = {
						order = 5,
						type = "toggle",
						name = PRIVATE_TABLE.L["IgnoreBossDebuffTitle"],
						desc = PRIVATE_TABLE.L["IgnoreBossDebuffDesc"],
						arg = "notMyAuras.debuffs.ignoreBoss",
						hidden = false,
					},
					IgnoreRem = {
						order = 6,
						type = "toggle",
						name = PRIVATE_TABLE.L["IgnoreRemDebuffTitle"],
						desc = PRIVATE_TABLE.L["IgnoreRemDebuffDesc"],
						arg = "notMyAuras.debuffs.ignoreRem",
						hidden = false,
					}
				},
			}
		}
	}
end

local OriginalLoadUnitDefaults = ShadowUF.LoadUnitDefaults
function ShadowUF:LoadUnitDefaults()
	OriginalLoadUnitDefaults(self)

	-- Initialize defaults
	for _, unit in pairs(self.unitList) do
		self.defaults.profile.units[unit].notMyAuras = {
			buffs = {
				fadeOut = false,
				fadeAmount = 1,
				desaturate = false,
				desaturateAmount = 0,
				ignoreBoss = true,
				ignoreRem = true,
			},
			debuffs = {
				fadeOut = false,
				fadeAmount = 1,
				desaturate = false,
				desaturateAmount = 0,
				ignoreBoss = true,
				ignoreRem = true,
			}
		}
	end
end
