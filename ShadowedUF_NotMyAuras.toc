## Interface: 100206
## Title: Shadowed UF Not My Auras
## Notes: Additional options for making the buffs & debuffs you've cast more prominent than those cast by other players (with the Shadowed Unit Frames addon)
## Author: Vysage
## Version: v1.0.1
## RequiredDeps: ShadowedUnitFrames

localization\enUS.lua
ShadowedUF_NotMyAuras.lua
